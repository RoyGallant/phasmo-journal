(function () {
    "use strict";

    class Evidence {
        constructor(name) {
            this.name = name || "Evidence";
            
            let value = 0;
            this.click = () => value = (value + 1) % 3;
            this.isFound = () => value === 1;
            this.isMissing = () => value === 2;
        }
    }

    class Ghost {
        constructor(name, evidences, requiredEvidences) {
            this.name = name || "Ghost";
            this.evidences = evidences || [];
            this.requiredEvidences = requiredEvidences || [];

            let value = 0;
            this.click = () => value = (value + 1) % 3;
            this.isCircled = () => value === 1;
            this.isStriked = () => value === 2;
        }

        matchesEvidences(foundEvidences, missingEvidences, numberHidden) {
            let isFoundEvidencesValid = foundEvidences.length === 0 || (foundEvidences.every(e => this.evidences.includes(e)) && foundEvidences.length + this.requiredEvidences.filter(e => !foundEvidences.includes(e)).length <= this.evidences.length - numberHidden);
            let isMissingEvidencesValid = this.evidences.filter(e => missingEvidences.includes(e)).length <= numberHidden;
            let isrequiredEvidencesValid = this.evidences.length < numberHidden || this.requiredEvidences.every(e => !missingEvidences.includes(e));
            return isFoundEvidencesValid && isMissingEvidencesValid && isrequiredEvidencesValid;
        }

        matchesBehaviors(behaviors) {
            let sum = behaviors.reduce((a, b) => a + b.getGhostWeight(this.name), 0);
            return sum;
        }
    }

    class Behavior {
        constructor(text, ghosts) {
            this.text = text;
            this.ghosts = ghosts;
            
            let selected = false;
            this.click = () => selected = !selected;
            this.isSelected = () => selected;
            this.getGhostWeight = ghost => this.ghosts[ghost] || 0;
        }
    }

    class App {
        constructor(evidences, ghosts, behaviors) {
            this.getHiddenEvidences = () => document.querySelector("#hiddenEvidences input:checked").value;
            this.evidences = evidences.map(e => new Evidence(e));
            this.ghosts = ghosts.map(g => new Ghost(g.name, g.evidences, g.requiredEvidences));
            this.behaviors = behaviors.map(b => new Behavior(b.text, b.ghosts));

            let update = () => {
                let found = this.evidences.filter(e => e.isFound()).map(e => e.name);
                let missing = this.evidences.filter(e => e.isMissing()).map(e => e.name);
                let hidden = this.getHiddenEvidences();
                let possibleGhosts = this.ghosts.filter(ghost => ghost.matchesEvidences(found, missing, hidden));
                let impossibleGhosts = this.ghosts.filter(ghost => !possibleGhosts.includes(ghost));
                this.evidences.forEach(evidence => evidence.element.classList.toggle("impossible", !evidence.isFound() && !evidence.isMissing() && (possibleGhosts.filter(ghost => ghost.isCircled()).some(ghost => !ghost.matchesEvidences(found.concat(evidence.name), missing, hidden)) || possibleGhosts.filter(ghost => !ghost.isStriked()).every(ghost => !ghost.matchesEvidences(found.concat(evidence.name), missing, hidden)))));
                this.behaviors.forEach(behavior => behavior.element.classList.toggle("irrelevant", possibleGhosts.every(ghost => behavior.getGhostWeight(ghost.name) === 0)));
                this.ghosts.forEach(ghost => {
                    ghost.element.classList.toggle("impossible", impossibleGhosts.includes(ghost));
                    let behaviorWeight = ghost.matchesBehaviors(this.behaviors.filter(b => !b.element.classList.contains("irrelevant") && b.isSelected()));
                    let [red, green, blue] = behaviorWeight > 0 ? [0, 255, 0] : [255, 0, 0];
                    ghost.element.style.backgroundColor = `rgba(${red}, ${green}, ${blue}, ${Math.min(1, Math.abs(behaviorWeight))})`;
                });
            };

            this.evidences.forEach(function (evidence) {
                evidence.element = document.createElement("button");
                evidence.element.innerText = evidence.name;
        
                evidence.element.addEventListener("click", function () {
                    evidence.click();
                    evidence.element.classList.toggle("found", evidence.isFound());
                    evidence.element.classList.toggle("missing", evidence.isMissing());
                    update();
                });
        
                document.getElementById("evidences").append(evidence.element);
            });

            this.ghosts.forEach(function (ghost) {
                ghost.element = document.createElement("button");
                ghost.element.innerText = ghost.name;
                document.getElementById("ghosts").append(ghost.element);
        
                ghost.element.addEventListener("click", function () {
                    ghost.click();
                    ghost.element.classList.toggle("circled", ghost.isCircled());
                    ghost.element.classList.toggle("striked", ghost.isStriked());
                    update();
                });
            });
            
            this.behaviors.forEach(function (behavior) {
                behavior.element = document.createElement("button");
                behavior.element.innerText = behavior.text;
                document.getElementById("behaviors").append(behavior.element);

                behavior.element.addEventListener("click", function () {
                    behavior.click();
                    behavior.element.classList.toggle("selected", behavior.isSelected());
                    update();
                });
            });

            document.querySelectorAll("#hiddenEvidences input").forEach(function (elem) {
                elem.addEventListener("change", function () {
                    update();
                });
            });
            update();
        }
    }

    const emf = "EMF-5";
    const dots = "D.O.T.S projector";
    const fingerprints = "Fingerprints"
    const orbs = "Ghost Orbs";
    const writing = "Ghost Writing";
    const spiritBox = "Spirit Box";
    const freezing = "Freezing Temperatures";

    const evidences = [emf, dots, fingerprints, orbs, writing, spiritBox, freezing];

    const spirit = {name: "Spirit", evidences: [emf, writing, spiritBox]};
    const wraith = {name: "Wraith", evidences: [emf, dots, spiritBox]};
    const phantom = {name:"Phantom", evidences: [dots, fingerprints, spiritBox]};
    const poltergeist = {name: "Poltergeist", evidences: [fingerprints, writing, spiritBox]};
    const banshee = {name: "Banshee", evidences: [dots, fingerprints, orbs]};
    const jinn = {name: "Jinn", evidences: [emf, fingerprints, freezing]};
    const mare = {name: "Mare", evidences: [orbs, writing, spiritBox]};
    const revenant = {name: "Revenant", evidences: [orbs, writing, freezing]};
    const shade = {name: "Shade", evidences: [emf, writing, freezing]};
    const demon = {name: "Demon", evidences: [fingerprints, writing, freezing]};
    const yurei = {name: "Yurei", evidences: [dots, orbs, freezing]};
    const oni = {name: "Oni", evidences: [emf, dots, freezing]};
    const yokai = {name: "Yokai", evidences: [dots, orbs, spiritBox]};
    const hantu = {name: "Hantu", evidences: [fingerprints, orbs, freezing], requiredEvidences: [freezing]};
    const goryo = {name: "Goryo", evidences: [emf, dots, fingerprints], requiredEvidences: [dots]};
    const myling = {name: "Myling", evidences: [emf, fingerprints, writing]};
    const onryo = {name: "Onryo", evidences: [orbs, spiritBox, freezing]};
    const twins = {name: "The Twins", evidences: [emf, spiritBox, freezing]};
    const raiju = {name: "Raiju", evidences: [emf, dots, orbs]};
    const obake = {name: "Obake", evidences: [emf, fingerprints, orbs], requiredEvidences: [fingerprints]};
    const mimic = {name: "The Mimic", evidences: [fingerprints, orbs, spiritBox, freezing], requiredEvidences: [orbs]};
    const moroi = {name: "Moroi", evidences: [writing, spiritBox, freezing], requiredEvidences: [spiritBox]};
    const deogen = {name: "Deogen", evidences: [dots, writing, spiritBox], requiredEvidences: [spiritBox]};
    const thaye = {name: "Thaye", evidences: [dots, orbs, writing]};

    const ghosts = [
        spirit, wraith, phantom, poltergeist, banshee, jinn, mare, 
        revenant, shade, demon, yurei, oni, yokai, hantu, goryo, myling,
        onryo, twins, raiju, obake, mimic, moroi, deogen, thaye
    ];
    
    const behaviors = [
        {text: "Footprints", ghosts: {"Wraith": -5}},
        {text: "No footprints", ghosts: {"Wraith": 1}},
        {text: "Airball", ghosts: {"Oni": -2}},
        {text: "Many ghost events", ghosts: {"Oni": 0.3, "Thaye": 0.2, "Shade": -0.5}},
        {text: "Moves slow", ghosts: {"Revenant": 0.4, "Thaye": 0.2, "Raiju": -0.2, "The Twins": 0.1}},
        {text: "Moves fast", ghosts: {"Hantu": 0.2, "Raiju": 0.1, "Moroi": 0.2, "Deogen": 0.4, "Thaye": 0.2, "Revenant": -0.1, "The Twins": 0.1}},
        {text: "Fast only when chasing", ghosts: {"Revenant": 0.5, "Jinn": 0.2}},
        {text: "Roaming", ghosts: {"The Twins": 0.2, "Shade": 0.1, "Goryo": -0.1}},
        {text: "Move speed varies", ghosts: {"The Twins": 0.1, "Moroi": 0.2, "Raiju": 0.3, "Hantu": 0.4, "Deogen": 0.2, "Thaye": 0.2, "The Mimic": 0.1}},
        {text: "Reacts to voice/electronics during hunt", ghosts: {"Yokai": -0.8}},
        {text: "Easy to crucifix", ghosts: {"Demon": 0.2}},
        {text: "Uses crucifix under lit candle", ghosts: {"Onryo": -0.8}},
        {text: "Heavy breathing in Spirit Box", ghosts: {"Deogen": 2}},
        {text: "Talking triggers hunt", ghosts: {"Yokai": 0.3}},
        {text: "Hunts 90 seconds after smudge", ghosts: {"Demon": 0.6, "Spirit": -0.7}},
        {text: "Screams on paramic", ghosts: {"Banshee": 1}},
        {text: "Hunts 120 seconds after smudge", ghosts: {"Demon": -0.1, "Spirit": -0.3}},
        {text: "Six fingers", ghosts: {"Obake": 1}},
        {text: "Hunts 180 seconds after smudge", ghosts: {"Demon": -0.3, "Spirit": 0.2}},
        {text: "Closes fully-open door", ghosts: {"Yurei": 0.2}},
        {text: "Hunts early", ghosts: {"Demon": 0.5, "Banshee": 0.1, "Yokai": 0.2, "Mare": 0.2, "Thaye": 0.2, "Onryo": 0.3, "Raiju": 0.2, "Shade": -0.6, "Deogen": -0.2}},
        {text: "Hunts late", ghosts: {"Deogen": 0.2, "Mare": 0.1, "Shade": 0.3, "Thaye": 0.1, "Demon": -0.1, "Onryo": -0.1}},
        {text: "D.O.T.S only on camera", ghosts: {"Goryo": 0.9}},
        {text: "Quiet footsteps", ghosts: {"Myling": 0.3}},
        {text: "Turns on breaker", ghosts: {"Hantu": -2, "Jinn": 0.1}},
        {text: "Turns off breaker", ghosts: {"Hantu": 0.2, "Jinn": -2}},
        {text: "Drains sanity faster", ghosts: {"Moroi": 0.2, "Yurei": 0.1, "Jinn": 0.1, "Poltergeist": 0.1}},
        {text: "Simultaneous interactions", ghosts: {"Poltergeist": 0.1, "The Twins": 0.3}},
        {text: "Unglitched ghost photo", ghosts: {"Phantom": 0.6}},
        {text: "Less active when near players", ghosts: {"Shade": 0.3, "Goryo": 0.1}},
        {text: "Glitched ghost photo", ghosts: {"Phantom": -5}},
        {text: "No chase speed-up", ghosts: {"Hantu": 0.1}},
        {text: "Sometimes missing fingerprints", ghosts: {"Obake": 0.2}},
        {text: "Ghost blinks slower", ghosts: {"Phantom": 0.3}},
        {text: "Silent EMF-2", ghosts: {"Wraith": 0.1, "Jinn": 0.1}}
    ];

    new App(evidences, ghosts, behaviors);
}());